import React from "react";
import { Platform, StyleSheet, Text, TouchableNativeFeedback, TouchableOpacity, View } from "react-native";

interface Props {
    title: string,
    position?: 'br' | 'bl',
    onPress: () => void
}

export const Fab = (props: Props) => {
    const { onPress, position = 'br', title } = props;

    const ios = () => {
        return (
            <TouchableOpacity
                onPress={onPress}
                activeOpacity={0.75}
                style={position === 'bl' ? styles.fabLocationBottomLeft : styles.fabLocationBottomRight}
            >
                <View style={styles.fab}>
                    <Text style={styles.fabText}>{title}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    const android = () => {
        return (
            <View style={position === 'bl' ? styles.fabLocationBottomLeft : styles.fabLocationBottomRight}>
                <TouchableNativeFeedback
                    background={TouchableNativeFeedback.Ripple('#28425B', false, 30)}
                    onPress={onPress}
                >
                    <View style={styles.fab}>
                        <Text style={styles.fabText}>{title}</Text>
                    </View>
                </TouchableNativeFeedback>
            </View>
        );
    }

    return Platform.OS === 'android' ? android() : ios();
}

const styles = StyleSheet.create({
    fabLocationBottomLeft: {
        bottom: 25,
        left: 25,
        position: 'absolute',
    },
    fabLocationBottomRight: {
        bottom: 25,
        position: 'absolute',
        right: 25
    },
    fab: {
        backgroundColor: '#5856D6',
        borderRadius: 100,
        elevation: 8,
        height: 60,
        justifyContent: 'center',
        shadowColor: '#000',
        shadowOffset: { height: 4, width: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 4.65,
        width: 60
    },
    fabText: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold'
    }
});